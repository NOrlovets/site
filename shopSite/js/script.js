$(document).ready(function () {
    $(".btn-info").click(function () {
        $(".search").fadeIn(300);
        positionPopup();
    });

    $(".search > span").click(function () {
        $(".search").fadeOut(300);
    });
});

$(document).ready(function () {
    $(".btn-primary").click(function () {
        $(".shop").fadeIn(300);
        positionPopup();
    });

    $(".shop > span").click(function () {
        $(".shop").fadeOut(300);
    });
});

$(".button").click(function() {
  $(this).toggleClass('transform-active');
});
